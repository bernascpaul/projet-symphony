<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(ProductRepository $productRepository)
    {
        $productsDate = $productRepository->findByDate();
        $productsPrice = $productRepository->findByPrice();

        return $this->render('index.html.twig', [
            'productsDate' => $productsDate,
            'productsPrice' => $productsPrice
        ]);
    }
}