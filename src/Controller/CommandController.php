<?php

namespace App\Controller;

use DateTime;
use App\Entity\Command;
use App\Form\CommandType;
use App\Repository\CommandRepository;
use App\Repository\ProductRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CommandController extends AbstractController
{
    /**
     * @Route("/command", name="command");
     */
    public function index(CommandRepository $commandRepository, SessionInterface $session, Request $request)
    {       
        $commands = $commandRepository->findAll();

        return $this->render('command/index.html.twig', [
            'commands' => $commands,
        ]);
    }

     /**
     * @Route("/command/{id}", name="command_show")
     */
    public function afficherCommand(CommandRepository $commandRepository, $id)
    {
        $command = $commandRepository->find($id);
        $total = 0;
        foreach($command->getProducts() as $one_product){
            $total += $one_product->getPrice();
        }
        return $this->render('command/show_command.html.twig', [
            'command' => $command,
            'total' => $total,
        ]);
    }
}