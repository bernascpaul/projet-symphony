<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProductController extends AbstractController
{
    /**
     * @Route("/product", name="product")
     */
    public function index(ProductRepository $productRepository, PaginatorInterface $paginator, Request $req)
    {
        $products = $productRepository->findAll();

        $pagination = $paginator->paginate($products, $req->query->getInt('page', 1), 10);
        return $this->render('product/index.html.twig', [
            'controller_name' => 'ProductController',
            'products' => $products
        ]);
    }

    /**
     * @Route("/product/{id}", name="product_show")
     */
    public function show(ProductRepository $productRepository, Request $req, $id)
    {
        $product = $productRepository->find($id);
        return $this->render('product/show.html.twig', [
            'product' => $product,
        ]);
    }
}
