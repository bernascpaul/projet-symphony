<?php

namespace App\Controller;

use App\Entity\Command;
use App\Form\CommandType;
use App\Repository\ProductRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PanierController extends AbstractController
{
    
    /**
     * @Route("/panier/add/{id}", name="panier_add")
     */
    public function add($id, SessionInterface $session, ProductRepository $productRepository)
    {
        $product = $productRepository->find($id);
        if(!$product) {
            return $this->json(['res' => 'nok'], 404);
        }
        else {
            $panier = $session->get('panier', []);
            // ...
            $panier[$id] = 1;
            // ...
            $session->set('panier', $panier);
            // ...
            return $this->json(['res' => 'ok'], 200);
        }
    }

    /**
     * @Route("/panier", name="panier")
     */
    public function afficherPanier(Request $request, SessionInterface $session, ProductRepository $productRepository)
    {
        $panier = $session->get('panier', []);
        $products = [];
        $total = 0;
        foreach($panier as $id => $quantity) {
            $product = $productRepository->find($id);
            $products[] = [
                'name' => $product->getName(),
                'price' => $product->getPrice(),
                'quantity' => $quantity,
                'id' => $id,
            ];
            foreach($products as $one_product){
                $total += $one_product['price'];
            }
        }

        $command = new Command();
        $formCommand = $this->createForm(CommandType::class, $command);
        $manager = $this->getDoctrine()->getManager();

        $formCommand->handleRequest($request);
        if($formCommand->isSubmitted()) {
            $command->setCreatedAt(new \Datetime);
            foreach($products as $one_product){
                $Produit_object = $productRepository->find($one_product['id']);
                $command->addProduct($Produit_object);
            }
            
            $manager->persist($command);
            $manager->flush();
            return $this->redirectToRoute('panier');
        }


        return $this->render('product/panier.html.twig', [
            'products' => $products,
            'total' => $total,
            'commandForm' => $formCommand->createView(),
        ]);
    }

    /**
     * @Route("/panier/delete/{id}", name="panier_delete")
     */
    public function deleteProduit($id, SessionInterface $session, Request $request)
    {
        $token = $request->request->get('token');
        $panier = $session->get('panier');
        if ($this->isCsrfTokenValid('delete-item', $token)) {
            if ($panier[$id]){
                unset($panier[$id]);
                $session->set('panier', $panier);
                return $this->redirectToRoute('panier');
            }
        }
    }
}